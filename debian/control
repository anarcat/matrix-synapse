Source: matrix-synapse
Maintainer: Matrix Packaging Team <pkg-matrix-maintainers@lists.alioth.debian.org>
Uploaders: Andrej Shadura <andrewsh@debian.org>
Section: net
Priority: optional
Build-Depends:
 debhelper (>= 9.20160709),
 dh-python,
 libjs-jquery,
 po-debconf,
 python3-all,
 python3-attr (>= 17.4.0),
 python3-bcrypt,
 python3-blist,
 python3-canonicaljson (>= 1.1.3),
 python3-daemonize,
 python3-frozendict (>= 1),
 python3-jsonschema (>= 2.5.1),
 python3-mock,
 python3-msgpack (>= 0.5.0),
 python3-nacl (>= 1.2.1),
 python3-netaddr (>= 0.7.18),
 python3-openssl (>= 0.14),
 python3-phonenumbers,
 python3-pil,
 python3-prometheus-client,
 python3-psutil,
 python3-pyasn1,
 python3-pymacaroons (>= 0.13.0),
 python3-pysaml2 (>= 4.0.0),
 python3-service-identity (>= 16.0.0),
 python3-setuptools (>= 0.6b3),
 python3-signedjson (>= 1.0.0),
 python3-six,
 python3-sortedcontainers,
 python3-treq (>= 18),
 python3-twisted (>= 18.7.0),
 python3-unpaddedbase64 (>= 1.0.1),
 python3-yaml
Standards-Version: 4.3.0
Homepage: https://matrix.org/docs/projects/server/synapse.html
Vcs-Browser: https://salsa.debian.org/matrix-team/matrix-synapse
Vcs-Git: https://salsa.debian.org/matrix-team/matrix-synapse

Package: matrix-synapse
Architecture: all
Depends:
 adduser,
 libjs-jquery,
 lsb-base (>= 3.0-6),
 libpython3-stdlib (<< 3.6.4~rc1-2~) | python3-distutils,
 python3-systemd,
 ${misc:Depends},
 ${python3:Depends}
Breaks:
 matrix-synapse-ldap3 (<< 0.1.3-2~)
Suggests:
 python3-txacme,
Recommends:
 python3-lxml,
 python3-psycopg2 (>= 2.6),
 python3-bleach (>= 1.4.2),
 python3-jinja2 (>= 2.9),
#  python3-priority,
#  python3-h2 (>= 3.0.0)
Description: Matrix reference homeserver
 Synapse is the reference Python/Twisted Matrix homeserver implementation.
 .
 Synapse is intended to showcase the concept of Matrix, and to let users run
 their own homeserver and generally help bootstrap the ecosystem.
 .
 Matrix is an open standard for interoperable, decentralised, real-time
 communication over IP. It can be used to power Instant Messaging, VoIP/WebRTC
 signalling, Internet of Things communication or anywhere where's a need for
 a standard HTTP API for publishing and subscribing to data whilst tracking the
 conversation history.
 .
 In Matrix, every user runs one or more Matrix clients, which connect through
 to a Matrix homeserver. The homeserver stores all their personal chat history
 and user account information, much as a mail client connects through to an
 IMAP/SMTP server.
